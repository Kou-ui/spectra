
import numpy as np
import numba
import numpy

from ... import Constants as Cst
from ... import Config as Cfg


from ...Atomic import Hydrogen

def compute_PI_cross_section(_ni, _meshCont):
    r""" """

    ## compute ratio of the transition energy to ionization energy
    _Eratio = ratio_Etran_to_Eionize(_ni[:], _meshCont[::])

    _PI_alpha = np.zeros(_Eratio.shape, dtype=np.double)
    for k in range(_Eratio.shape[0]):
        _PI_alpha[k,:] = Hydrogen.PI_cross_section(_ni[k], _Eratio[k,:], 1)

    return _PI_alpha

@numba.vectorize([f'{Cfg.dtsINT_}({Cfg.dtsINT_})'], nopython=True)
def get_level_n( g ):
    r"""
    compute the quantum number n given statistical weight g

    Parameters
    -----------
    g : int, [:math:`-`]
        statistical weight

    Returns
    --------
    n : int [:math:`-`]
        quantum number n
    """
    if g == 1:
        assert False
    else:
        return int( np.sqrt(g//2) )

def ratio_Etran_to_Eionize( ni_arr, wave_arr ):
    r"""

    """

    nCont = wave_arr.shape[0]
    ratio = np.ones(wave_arr.shape, np.double)

    for k in range(nCont):
        E_ionize = Cst.E_Rydberg_ / ni_arr[k]**2
        E_tran =  Cst.h_ * Cst.c_ / wave_arr[k,:]
        E_tran[:] +=  E_ionize - E_tran[0]

        ratio[k,:] = E_tran[:] / E_ionize

    return ratio

def collisional_broadening(_ni, _nj, _Te, _Ne):
    r""" """

    ## we assume the HI ground level popylation to 1.E10
    _gamma  = Hydrogen.collisional_broadening_Res_and_Van(_ni, _nj, 1.E10, _Te)
    _gamma += Hydrogen.collisional_broadening_LinearStark(_ni, _nj, _Ne)

    return _gamma

#-----------------------------------------------------------------------------
# numba optimizations
#-----------------------------------------------------------------------------

if Cfg.isJIT_:
    ratio_Etran_to_Eionize = numba.njit( [
        f'{Cfg.dtsDOUBLE_}[:,:]({Cfg.dtsINT_}[:],{Cfg.dtsDOUBLE_}[:,:])'
        ] )(ratio_Etran_to_Eionize)

    collisional_broadening = numba.vectorize([
        f'{Cfg.dtsDOUBLE_}({Cfg.dtsINT_},{Cfg.dtsINT_},{Cfg.dtsDOUBLE_},{Cfg.dtsDOUBLE_})'
        ], nopython=True) (collisional_broadening)
