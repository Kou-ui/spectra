
#-----------------------------------------------------------------------------
# warnings:
#   20201116 kouui
#     - by defalut the line absorption profile is Gaussian shape so
#       no damping effect in line wings
#       ```B_Jbar()
#       _meshInfo[3] = 2
#       ```
#     - in LevelN.collisional_broadening(), ground hydrogen population is set(fixed) to 1E10
#
#-----------------------------------------------------------------------------


__VERSION__ = \
"""0.1.1 2020/11/16 kouui
    - Te and Tr identification in bf_R_rate() and bf_R_rate_loop()
    - 0.5*intensity --> 1.0*intensity in the integration in B_Jbar_CRD()

   0.1.2 2021/05/08 kouui
    - func B_Jbar : _meshInfo assigment
    - func B_Jbar : _Gamma = numpy.atleast_1d( _Gamma )
"""

import numpy
import numba

from numpy import exp
#from math import exp
#-----------------------------------------------------------------------------
# module namespace
#-----------------------------------------------------------------------------
from ... import Constants as Cst
from ... import Config as Cfg


from ...Atomic import Collision, PhotoIonize, LTELib, SEsolver, BasicP, SEsolver
from ...RadiativeTransfer import Profile
from ...Math import Integrate
from ...Atomic import Hydrogen
from ..Hydrogen import LevelN

from ...Structure.MyTypes import T_ATOM, T_DATA

import matplotlib.pyplot as plt



#-----------------------------------------------------------------------------
# high level functions with class object as function argument
#-----------------------------------------------------------------------------

def ni_nj_LTE(_atom, _Te, _Ne):
    r""" """

    ## convert _Te, _Ne into numpy array
    _Te = numpy.atleast_1d( _Te )
    _Ne = numpy.atleast_1d( _Ne )


    ## grab arrays
    _Level = _atom.Level
    _Line  = _atom.Line
    _nLevel = _atom.nLevel
    _nLine = _atom.nLine
    if _atom.hasContinuum:
        _Cont  = _atom.Cont
        _nCont = _atom.nCont
        _nTran = _nLine + _nCont
    else:
        _nTran = _nLine

    ## initilize _nj_by_ni
    _nj_by_ni = numpy.empty( (_nTran,)+_Te.shape, dtype=Cfg.dtDOUBLE_ )

    ## for line transitions
    _gi = _Line['gi'][:]
    _gj = _Line['gj'][:]
    _Eji = Cst.h_ * _Line['f0'][:]

    for k in range(0, _nLine):
        _nj_by_ni[k,:] = LTELib.Boltzmann_distribution(_gi[k], _gj[k], _Eji[k], _Te[:])

    _idxI_L = _atom.Line['idxI'][:]
    _idxJ_L = _atom.Line['idxJ'][:]

    #_stage = _atom.Level['stage'][:]

    ## if there is continuum transition
    if _atom.hasContinuum:

        _gi = _Cont['gi'][:]
        _gj = _Cont['gj'][:]
        _chi = Cst.h_ * _Cont['f0'][:]

        for k in range(_nCont):
            _nj_by_ni[k+_nLine,:] = LTELib.Saha_distribution(_gi[k], _gj[k], _chi[k], _Ne[:], _Te[:])

        _idxI = numpy.append( _Line['idxI'][:],_Cont['idxI'][:] )
        _idxJ = numpy.append( _Line['idxJ'][:],_Cont['idxJ'][:] )
    else:
        _idxI = _Line['idxI'][:]
        _idxJ = _Line['idxJ'][:]

    _isGround = _Level['isGround'][:]
    ## compute _ni
    _ni = nj_by_ni_To_ni(_nj_by_ni[:,], _idxI[:], _idxJ[:], _isGround[:], _nLine)

    return _ni, _nj_by_ni[:_nLine,:], _nj_by_ni[_nLine:,:]

def bf_R_rate(_atom, _Te, _nj_by_ni_Cont, _Tr=None, _backRad=None):
    r""" """
    ## convert _Te, _Ne into numpy array
    _Te = numpy.atleast_1d( _Te )


    _Cont = _atom.Cont
    #_nCont = _atom.nCont
    _meshCont = _atom.Mesh.Cont[:,:]

    if _Tr is None:
        if _backRad is None:
            assert False, "_backRad=None, no background intensity available for interpolation"
        else:
            _PI_I = PhotoIonize.interpolate_PI_intensity(_backRad[:,:], _meshCont[:,:])

    else:
        _PI_I = LTELib.Planck_cm(_meshCont[:,:],_Tr)

    #-------------------------------------------------------------------------
    # we compute/interpolate photoionizatoin cross section only once
    # and assume that while suffering Doppler shift
    #    - continuum wavelength mesh might shift
    #        (but for the sake of simplicity, we assume they do not shift)
    #    - photoionizatoin cross section keep constant
    #-------------------------------------------------------------------------

    _PI_alpha = _atom.PI_alpha[:,:]

    ## ! could not be optimized to Te-Ne-array
    _Rik, _Rki_stim, _Rki_spon = bf_R_rate_loop(_waveMesh=_meshCont[:,:],
                                       _Jnu=_PI_I[:,:],
                                       _alpha=_PI_alpha[:,:],
                                       _Te=_Te,
                                       _nj_by_ni_Cont=_nj_by_ni_Cont)

    return _Rik, _Rki_stim, _Rki_spon

def B_Jbar(_atom, _Te, _Vt=None, _Vd=None, _Ne=None, _backRad=None, _Tr=None):
    r""" """
    ## convert _Te, _Ne into numpy array
    _Te = numpy.atleast_1d( _Te )


    _Level = _atom.Level
    _Line  = _atom.Line
    _nLine = _atom.nLine

    _MeshCoe = _atom.Mesh.Coe

    _wave_mesh_cm_shifted_arr = []
    _absorb_Prof_cm_arr = []
    #-------------------------------------------------------------------------
    # case with background intensity
    #-------------------------------------------------------------------------
    if _Tr is None:

        _Mass = _atom.Mass

        ## assert input arguments
        if _Vt is None:
            assert False, "_Vt is None"
        if _Vd is None:
            assert False, "_Vd is None"
        if _backRad is None:
            assert False, "_backRad is None"
        if _Ne is None:
            assert False, "_Ne is None"

        ## convert spatial physics parameter into array
        _Vt = numpy.atleast_1d( _Vt )
        _Vd = numpy.atleast_1d( _Vd )
        _Ne = numpy.atleast_1d( _Ne )

        _Te_1D = _Te.reshape(-1)
        _Ne_1D = _Ne.reshape(-1)
        _Vt_1D = _Vt.reshape(-1)
        _Vd_1D = _Vd.reshape(-1)

        _nSpatial = _Te_1D.shape[0]

        ## initilize result array
        _Bji_Jbar = numpy.empty( (_nLine,)+_Te_1D.shape, dtype=Cfg.dtDOUBLE_ )
        _Bij_Jbar = numpy.empty_like( _Bji_Jbar, dtype=Cfg.dtDOUBLE_ )

        ## temp array to store mesh information
        _meshInfo = numpy.empty(4, dtype=Cfg.dtDOUBLE_)
        ## loop over line transition
        for k in range(_nLine):

            ## ignore lines with Aji smaller than 1.E-3
            if _Line['AJI'][k] < 1.E-3:
                _Bji_Jbar[k,:] = 0.
                _Bij_Jbar[k,:] = 0.
                continue

            ## collisional broadening line width
            if _atom.ATOM_TYPE == T_ATOM.HYDROGEN:
                _Gamma = _Line["Gamma"][k] + \
                         LevelN.collisional_broadening(_Line['ni'][k],_Line['nj'][k], _Te_1D, _Ne_1D)
            else:
                ## function not yet implemented for other types of atom
                _Gamma = _Line["Gamma"][k]
            _Gamma = numpy.atleast_1d( _Gamma )

            # ProfileType : 0 --> Voigt, else --> Gaussian
            _isRad = False
            for _kRad in range( _MeshCoe['lineIndex'][:].shape[0] ):

                if k == _MeshCoe['lineIndex'][_kRad]:
                    _meshInfo[0] = int( _MeshCoe['nLambda'][ _kRad ] )
                    _meshInfo[1] = _MeshCoe['qcore'][ _kRad ]
                    _meshInfo[2] = _MeshCoe['qwing'][ _kRad ]
                    _meshInfo[3] = _MeshCoe['ProfileType'][ _kRad ]
                    _isRad = True
                    break
            if not _isRad:
                _meshInfo[0] = 41 # 21
                _meshInfo[1] = 2.5
                _meshInfo[2] = 10.
                _meshInfo[3] = 2

#            if k in _MeshCoe['lineIndex'][:]:
#                _kRad = np.where( _MeshCoe['lineIndex'][k] )[0][0]
#                _meshInfo[0] = _MeshCoe['nLambda'][ kRad ]
#                _meshInfo[1] = _MeshCoe['qcore'][ kRad ]
#                _meshInfo[2] = _MeshCoe['qwing'][ kRad ]
#                _meshInfo[3] = _MeshCoe['ProfileType'][ kRad ]
#            else:
#                _meshInfo[0] = 41 # 21
#                _meshInfo[1] = 2.5
#                _meshInfo[2] = 10.
#                _meshInfo[3] = 2

            _Bij = _Line['BIJ'][k]
            _Bji = _Line['BJI'][k]
            _w0  = _Line['w0'][k]
            #print( _meshInfo )
            _wave_mesh_cm_shifted_arr1, _absorb_Prof_cm_arr1 = B_Jbar_CRD(_Bij, _Bji, _w0, _Mass, _meshInfo[:], _backRad[:,:],
                 _Gamma[:], _Te_1D[:], _Vt_1D[:], _Vd_1D[:], _Bij_Jbar[k,:], _Bji_Jbar[k,:])
            _wave_mesh_cm_shifted_arr.append( _wave_mesh_cm_shifted_arr1 )
            _absorb_Prof_cm_arr.append( _absorb_Prof_cm_arr1 )

        ## reshape back to the original spatial dimension
        _spatial_shape = _Te.shape
        _Bji_Jbar = _Bji_Jbar.reshape(_nLine,*_spatial_shape)
        _Bij_Jbar = _Bij_Jbar.reshape(_nLine,*_spatial_shape)

        ## return list for general use case
        #_wave_mesh_cm_shifted_arr = numpy.array( _wave_mesh_cm_shifted_arr )
        #_absorb_Prof_cm_arr = numpy.array( _absorb_Prof_cm_arr )

    #-------------------------------------------------------------------------
    # case of assuming radiation temperature
    #-------------------------------------------------------------------------
    else:
        ## ! could be optimized to Te-Ne-array
        _Jbar = LTELib.Planck_cm(_Line["w0"][:], _Tr)

        ## initilize result array
        _Bji_Jbar = numpy.empty( (_nLine,)+_Te.shape, dtype=Cfg.dtDOUBLE_ )
        _Bij_Jbar = numpy.empty_like( _Bji_Jbar, dtype=Cfg.dtDOUBLE_ )

        _extra_dim = (1,)*_Te.ndim
        _Bji_Jbar[:] = (_Line["BJI"][:] * _Jbar[:]).reshape(_nLine,*_extra_dim)
        _Bij_Jbar[:] = (_Line["BIJ"][:] * _Jbar[:]).reshape(_nLine,*_extra_dim)

    return _Bij_Jbar, _Bji_Jbar, _wave_mesh_cm_shifted_arr, _absorb_Prof_cm_arr

def get_Cij(_atom, _Te):
    r""" """
    ## convert _Te, _Ne into numpy array
    _Te = numpy.atleast_1d( _Te )
    _Te_1D = _Te.reshape(-1)

    ## grab arrays
    _nLine = _atom.nLine
    if _atom.hasContinuum:
        _nCont = _atom.nCont
        _nTran = _nLine + _nCont
    else:
        _nTran = _nLine

    _Cij = numpy.empty( (_nTran,)+_Te_1D.shape, dtype=Cfg.dtDOUBLE_ )

    #------
    # for line transition
    #------
    if _atom.ATOM_DATA_TYPES.CE == T_DATA.INTERPOLATE:

        _Omega_table = _atom.CE.Omega_table[:,:]
        _Te_table    = _atom.CE.Te_table[:]
        _f1        = _atom.CE.Coe['f1'][:]
        _f2        = _atom.CE.Coe['f2'][:]
        _gi        = _atom.CE.Coe['gi'][:]
        _dEij      = _atom.CE.Coe['dEij'][:]

        for k in range(_nLine):
            _omega = Collision.interp_Omega(_Omega_table[k,:], _Te_1D[:],
                                           _Te_table[:], _f1[k], _f2[k])

            _Cij[k,:] = Collision.get_CE_rate_coe(_omega[:], _Te_1D[:], _gi[k],_dEij[k] )



    elif _atom.ATOM_DATA_TYPES.CE == T_DATA.CALCULATE:

        _Line  = _atom.Line

        if _atom.ATOM_TYPE == T_ATOM.HYDROGEN:

            _ni = _Line['ni'][:]
            _nj = _Line['nj'][:]
            for k in range(_nLine):
                _Cij[k,:] = Hydrogen.CE_rate_coe(_ni[k], _nj[k], _Te_1D[:])

        else:
            assert False

    else:
        assert False


    if _atom.hasContinuum:
        #------
        # for continuum transition
        #------

        if _atom.ATOM_DATA_TYPES.CI == T_DATA.INTERPOLATE:

            _Omega_table = _atom.CI.Omega_table[:,:]
            _Te_table    = _atom.CI.Te_table[:]
            _f2        = _atom.CI.Coe['f2'][:]
            _dEij      = _atom.CI.Coe['dEij'][:]

            for k in range(_nCont):
                _omega = Collision.interp_Omega(_Omega_table[k,:], _Te_1D[:],
                                               _Te_table[:], 1., _f2[k])

                _Cij[k+_nLine,:] = Collision.get_CI_rate_coe(_omega[:], _Te_1D[:], _dEij[k] )


        elif _atom.ATOM_DATA_TYPES.CI == T_DATA.CALCULATE:

            _Cont  = _atom.Cont

            if _atom.ATOM_TYPE == T_ATOM.HYDROGEN:
                _ni = _Cont['ni'][:]
                for k in range(_nCont):
                    _Cij[k+_nLine,:]= Hydrogen.CI_rate_coe(_ni[k], _Te_1D[:])

            else:
                assert False

        else:
            assert False

    return _Cij.reshape(_nTran,*_Te.shape)

def solve_SE(_atom, _Ne, _Cji, _Cij, _Bji_Jbar, _Bij_Jbar, _Rki_spon, _Rki_stim, _Rik):
    r""" """

    r""" """
    ## convert _Te, _Ne into numpy array
    _Ne = numpy.atleast_1d( _Ne )
    _Ne_1D    = _Ne.reshape(-1)
    _nSpatial = _Ne.size

    _nLevel = _atom.nLevel
    _nCont  = _atom.nCont
    _nLine  = _atom.nLine
    _nTran  = _nCont + _nLine

    _idxI = numpy.append(_atom.Line.idxI[:], _atom.Cont.idxI[:])
    _idxJ = numpy.append(_atom.Line.idxJ[:], _atom.Cont.idxJ[:])

    _Aji  = _atom.Line.AJI[:]

    #----
    # reshape spatial 1D
    #----
    _Rki_spon_1D = _Rki_spon.reshape(_nCont, -1)
    _Rki_stim_1D = _Rki_stim.reshape(_nCont, -1)
    _Rik_1D      = _Rik.reshape(_nCont, -1)
    _Bij_Jbar_1D = _Bij_Jbar.reshape(_nLine, -1)
    _Bji_Jbar_1D = _Bji_Jbar.reshape(_nLine, -1)
    _Cij_1D      = _Cij.reshape(_nTran, -1)
    _Cji_1D      = _Cji.reshape(_nTran, -1)

    _n_SE  = numpy.empty((_nLevel, _nSpatial), dtype=Cfg.dtDOUBLE_)


    for k in range(_nSpatial):

        _Rji_spon = numpy.append(_Aji[:], _Rki_spon_1D[:,k])
        _Rji_stim = numpy.append(_Bji_Jbar_1D[:,k], _Rki_stim_1D[:,k])
        _Rij      = numpy.append(_Bij_Jbar_1D[:,k], _Rik_1D[:,k])

        _n_SE[:,k] = solve_SE__(_nLevel, _idxI, _idxJ,
                      _Cji_1D[:,k], _Cij_1D[:,k], _Rji_spon, _Rji_stim, _Rij, _Ne_1D[k])

    return _n_SE.reshape(_nLevel, *_Ne.shape)

#-----------------------------------------------------------------------------
# Array level functions which could be optimized by numba
#-----------------------------------------------------------------------------
def nj_by_ni_To_ni(_nj_by_ni, _idxI, _idxJ, _isGround, _nLine):
    r""" """

    _nLevel = _isGround.shape[0]
    _nTran  = _idxI.shape[0]

    _ni = numpy.empty((_nLevel,)+_nj_by_ni.shape[1:], dtype=Cfg.dtDOUBLE_)
    _ni[0,:] = 1.

    for k in range(_nLine, _nTran):
        i = _idxI[k]
        j = _idxJ[k]
        if _isGround[ i ]:
            _ni[ j ] = _nj_by_ni[ k ] * _ni[ i ]

    for k in range(0, _nLine):
        i = _idxI[k]
        j = _idxJ[k]
        if _isGround[ i ]:
            _ni[ j ] = _nj_by_ni[ k ] * _ni[ i ]

    return _ni[:,:] / _ni[:,:].sum(axis=0)

def bf_R_rate_loop(_waveMesh, _Jnu, _alpha, _Te, _nj_by_ni_Cont):
    r""" """

    _nCont = _waveMesh.shape[0]
    ## flatten spatial ndarray
    _nSpatial = _Te.size
    _Rik      = numpy.empty( (_nCont,_nSpatial), dtype=Cfg.dtDOUBLE_ )
    _Rki_stim = numpy.empty_like( _Rik, dtype=Cfg.dtDOUBLE_ )
    _Rki_spon = numpy.empty_like( _Rik, dtype=Cfg.dtDOUBLE_ )

    _Te_1D = _Te.reshape(-1)
    _nj_by_ni_Cont_2D = _nj_by_ni_Cont.reshape(_nj_by_ni_Cont.shape[0],-1)

    ## loop over continuum transition
    for kL in range(_nCont):
        ## loop over flattened 1D spatial points
        for kX in range(_nSpatial):

            _res = PhotoIonize.bound_free_radiative_transition_coefficient(
                                wave = _waveMesh[kL,::-1],
                                J = _Jnu[kL,::-1],
                                alpha = _alpha[kL,::-1],
                                Te = _Te_1D[kX],
                                nk_by_ni_LTE=_nj_by_ni_Cont_2D[kL,kX])

            _Rik[kL,kX]      = _res[0]
            _Rki_stim[kL,kX] = _res[1]
            _Rki_spon[kL,kX] = _res[2]

    ## convert back to the shape we want
    _shape = (_nCont,) + _Te.shape
    return _Rik.reshape(_shape), _Rki_stim.reshape(_shape), _Rki_spon.reshape(_shape)

def solve_SE__(_nLevel, _idxI, _idxJ, _Cji, _Cij, _Rji_spon, _Rji_stim, _Rij, _Ne):
    r""" """

    _Cmat = numpy.zeros((_nLevel, _nLevel), dtype=Cfg.dtDOUBLE_)
    SEsolver.setMatrixC(_Cmat=_Cmat[:,:],
                        _Cji=_Cji[:],
                        _Cij=_Cij[:],
                        _idxI=_idxI, _idxJ=_idxJ, _Ne=_Ne)

    _Rmat = numpy.zeros((_nLevel, _nLevel), dtype=Cfg.dtDOUBLE_)
    SEsolver.setMatrixR(_Rmat=_Rmat[:,:],
                        _Rji_spon=_Rji_spon[:],
                        _Rji_stim=_Rji_stim[:],
                        _Rij=_Rij[:],
                        _idxI=_idxI, _idxJ=_idxJ)
    _n_SE = SEsolver.solveSE(_Rmat=_Rmat[:,:], _Cmat=_Cmat[:,:])

    return _n_SE

def B_Jbar_CRD(_Bij, _Bji, _w0, _Mass, _meshInfo, _backRad, _Gamma, _Te_1D, _Vt_1D, _Vd_1D, _Bij_Jbar_1D, _Bji_Jbar_1D):
    r"""
    _meshInfo[0] : _nLambda
    _meshInfo[1] : _qcore
    _meshInfo[2] : _qwing
    _meshInfo[3] : _ProfileType
    """
    ## in Doppler width unit
    #if _meshInfo[2] < 0.1:
        ## if qcore, qwing is not tabulated, calculate them
        #_dummy_dopWidth_cm = BasicP.get_Doppler_width(_w0, 1.E4, 1.E5, _Mass)
        ## default qcore covers -2.5 ~ +2.5
        ## default qwing covers -5.0A ~ +5.0A
        #_meshInfo[1] = 0.5 / _dummy_dopWidth_cm
        #_meshInfo[2] = 5.0 / (_dummy_dopWidth_cm * 1.E8 )
    _wave_mesh = Profile.makeLineMesh_Full( int(_meshInfo[0]), _meshInfo[1], _meshInfo[2] )

    ## loop over spatial points
    _nSpatial = _Te_1D.shape[0]
    _nWavelength = _wave_mesh.shape[0]
    _wave_mesh_cm_shifted_arr = numpy.empty((_nSpatial,_nWavelength))
    _absorb_Prof_cm_arr = numpy.empty((_nSpatial,_nWavelength))
    for kX in range(_nSpatial):

        ## Doppler width in [cm]
        _dopWidth_cm = BasicP.get_Doppler_width(_w0, _Te_1D[kX], _Vt_1D[kX], _Mass)
        ## absorption profile
        if int(_meshInfo[3]) == 0: # Voigt
            _f0 = Cst.c_ / _w0
            _dopWidth_hz = _dopWidth_cm*_f0/_w0
            _a = _Gamma[kX] / ( 4 * Cst.pi_ * _dopWidth_hz )
            _absorb_Prof_cm = Profile.Voigt(_a, _wave_mesh[:]) / _dopWidth_cm
        else:                 # Gaussian
            _absorb_Prof_cm = Profile.Gaussian(_wave_mesh[:]) / _dopWidth_cm
        _absorb_Prof_cm_arr[kX,:] = _absorb_Prof_cm[:]

        ## shift wavelength mesh
        #_wave_mesh_shifted = _wave_mesh[:] - (_w0*_Vd/Cst.c_)/_dopWidth_cm
        #_wave_mesh_cm_shifted = _wave_mesh_shifted[:] * _dopWidth_cm
        _wave_mesh_cm =  _wave_mesh[:] * _dopWidth_cm + _w0
        _wave_mesh_cm_shifted = _wave_mesh_cm[:] + (_w0*_Vd_1D[kX]/Cst.c_)
        _wave_mesh_cm_shifted_arr[kX,:] = _wave_mesh_cm_shifted[:]

        ## interpolate background intensity
        _I_cm_interp = numpy.interp( _wave_mesh_cm_shifted[:], _backRad[0,:], _backRad[1,:] )

        ## integrate Jbar
        #_integrand = 0.5 * _I_cm_interp[:] * _absorb_Prof_cm[:]
        #: the atlas data is extractly the outgoing intensity
        #  therefore it is unnecessary to multiply another 0.5
        _integrand = _I_cm_interp[:] * _absorb_Prof_cm[:]
        _Jbar0 = Integrate.Trapze(_integrand[:], _wave_mesh_cm_shifted[:])
        _Bij_Jbar_1D[kX] = _Bij * _Jbar0
        _Bji_Jbar_1D[kX] = _Bji * _Jbar0

        ##--------------------------------------------------------------------
        # debug
        ##--------------------------------------------------------------------
        ##plt.plot(_wave_mesh_cm_shifted[:], _I_cm_interp[:], 'o', label="interpolated intensity")
        ##_int_tr = LTELib.Planck_cm(_w0, 6E3)
        ##plt.plot(_wave_mesh_cm_shifted[:], [_int_tr,]*_I_cm_interp[:].size, lw=0.5, label="Planck intensity")
        ###plt.plot(_wave_mesh_cm_shifted[:], _absorb_Prof_cm[:], "absorption profile")
        ##plt.plot(_backRad[0,:], _backRad[1,:], lw=0.5, label="original intensity")
        ##plt.xlim(_wave_mesh_cm[0]-1E-8, _wave_mesh_cm[-1]+1E-8)
        ##plt.ylim(min(_I_cm_interp[:].min(),_int_tr)*0.7, max(_I_cm_interp[:].max(),_int_tr)*1.3)
        ##ax2 = plt.gca().twinx()
        ##ax2.plot(_wave_mesh_cm_shifted[:], _absorb_Prof_cm)
        ##plt.pause(0.05)
        ##--------------------------------------------------------------------

    return _wave_mesh_cm_shifted_arr, _absorb_Prof_cm_arr



#-----------------------------------------------------------------------------
# numba optimization
#-----------------------------------------------------------------------------
if Cfg.isJIT_:
    nj_by_ni_To_ni = numba.njit( nj_by_ni_To_ni )
    bf_R_rate_loop = numba.njit( bf_R_rate_loop )

#    B_Jbar_CRD = numba.njit( [
#        f'({Cfg.dtsDOUBLE_},{Cfg.dtsDOUBLE_},{Cfg.dtsDOUBLE_},{Cfg.dtsDOUBLE_},{Cfg.dtsDOUBLE_}[:],{Cfg.dtsDOUBLE_}[:,:],{Cfg.dtsDOUBLE_}[:],{Cfg.dtsDOUBLE_}[:],{Cfg.dtsDOUBLE_}[:],{Cfg.dtsDOUBLE_}[:],{Cfg.dtsDOUBLE_}[:],{Cfg.dtsDOUBLE_}[:])'
#        ] )(B_Jbar_CRD)
