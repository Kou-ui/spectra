
#-----------------------------------------------------------------------------
# modification history
#   20201230 kouui
#     -
#
#-----------------------------------------------------------------------------
# warnings:
#
#-----------------------------------------------------------------------------

from numpy import exp

def cloud_model(S, tau):
    r"""
    Parameters
    -----------
    S : float, array-like
        source function

    tau : float, array-like
        optical thickness as a function of wavelength
    """

    raise NotImplementedError('')
