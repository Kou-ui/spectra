
#-----------------------------------------------------------------------------
# modification history
#   2021/02/20 kouui
#     - added assertion of `hasattr` in the `.setValues` methods in
#       class `Rates`, `LineProfs` and `LevelPop`
#   2021/05/13 kouui
#     - added support for non-uniform wavelength meshgrid
#        wl, aprof, prof in class LineProfs are now list instead of array
#
#-----------------------------------------------------------------------------
# warnings:
#   20210220 kouui
#     - class `Rates`, `LineProfs` and `LevelPop` had better be replaced
#       with namedtuple or @dataclass(only applied with python>=3.7)
#
#-----------------------------------------------------------------------------
# class:
#   - Rates ()
#   - LineProfs ()
#   - LevelPop ()
# function:
#   - SE_population_NhTe (atom, Nh, Te, backRad, Vd, Vt)
#   - SE_population_NeTe (atom, Ne, Te, backRad, Vd, Vt)
#-----------------------------------------------------------------------------

import numpy as np

from ...Atomic import Collision, SEsolver, Opacity
#from ...Util import Class
#from ...Visual import Grotrian, Plotting
from ...Atomic import LTELib
from ...Function.StatisticalEquilibrium import LibSpatialVectorized as SELib
#from ...Atomic import Hydrogen

#from ...Math import Integrate

from ... import Constants as Cst
from ... import Config as Cfg


# calculate SE populations

#-----   Rates class  ----
class Rates:
    def __init__(self):
        self.Rik = 0.
        self.Rki_stim = 0.
        self.Rki_spon = 0.
        self.Bij_Jbar = 0.
        self.Bji_Jbar = 0.
        self.Cij = 0.
        self.Cji = 0.

    def setValues(self, **kwargs):
        for key,value in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, value)
            else:
                print(f"no attribute {key} in class Rates")
        return True

#-----   Line profiles class  ----
class LineProfs:
    def __init__(self):
        self.wl = 0.       #  wavelength mesh : length nLine list of [nWl,] array  [cm]
        self.aprof = 0.    #  absorption profile : length nLine list of [nWl,] array
        self.prof = 0.     #  output profile : length nLine list of [nWl,] array
        self.tau0 = 0.     #  central tau  [nLine]
        self.intens = 0.   #  integrated intensity  [nLine]

    def setValues(self, **kwargs):
        for key,value in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, value)
            else:
                print(f"no attribute {key} in class LineProfs")
        return True

#-----   Level population class  ----
class LevelPop:
    def __init__(self):
        self.LTE = 0.
        self.SE = 0.

    def setValues(self, **kwargs):
        for key,value in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, value)
            else:
                print(f"no attribute {key} in class LevelPop")
        return True


#--- calculate SE populations
def SE_population_NhTe(atom, Nh, Te, backRad, Vd, Vt):
# return
#       n_SE[n_Level+1]        -- population
#       Ne                     -- electron density
#       wave_mesh_arr[n_Line, n_wl]      -- wavelength  [cm]
#       absorb_prof_arr[n_Line, n_wl]    -- line profiles, normalized as integ_wl = 1

    Ne  = 0.5 * Nh  # [cm^{-3}]
    Ne0 = 1E-4 * Nh # イオン化バルブ効果
    nIter = 0

    while True:
        nIter += 1

        n_LTE , nj_by_ni_Line, nj_by_ni_Cont = SELib.ni_nj_LTE(atom, Te, Ne)

        Rik, Rki_stim, Rki_spon = SELib.bf_R_rate(atom, Te, nj_by_ni_Cont, _backRad=backRad)
        Bij_Jbar, Bji_Jbar, wave_mesh_arr, absorb_prof_arr = SELib.B_Jbar(atom, Te, _Vt=Vt, _Vd=Vd, _Ne=Ne, _backRad=backRad)
        Cij = SELib.get_Cij(atom, Te)
        Cji = Collision.Cij_to_Cji( Cij, np.append(nj_by_ni_Line,nj_by_ni_Cont, axis=0) )

        n_SE = SELib.solve_SE(atom, Ne, Cji, Cij, Bji_Jbar, Bij_Jbar, Rki_spon, Rki_stim, Rik)

        Ne_SE = Ne0 + Nh * n_SE[-1,0]
        Ne_new = 0.5 * (Ne_SE + Ne)
        if (abs(Ne_new-Ne) / Ne) < 1E-2:    # <---
            Ne = Ne_new
            break
        else:
            Ne = Ne_new

    # print(f"Number of iteration   = {nIter}")
    wave_mesh_list = [ arr.squeeze() for arr in wave_mesh_arr ]
    absorb_prof_list = [ arr.squeeze() for arr in absorb_prof_arr ]
    n_SE = np.array(n_SE).squeeze()
    n_LTE = np.array(n_LTE).squeeze()

    rate = Rates()
    rate.setValues(Rik = Rik, Rki_stim = Rki_stim, Rki_spon = Rki_spon, Bij_Jbar = Bij_Jbar, Bji_Jbar = Bji_Jbar, Cij = Cij, Cji = Cji)

    l = LineProfs()
    nLine = len( wave_mesh_list )
    prof = [np.zeros(arr.shape[-1]) for arr in wave_mesh_list]#np.zeros([nLine,nWl])
    tau0 = np.zeros(nLine)
    intens = np.zeros(nLine)
    l.setValues(wl = wave_mesh_list, aprof = absorb_prof_list, prof = prof, tau0 = tau0, intens = intens)

    pop = LevelPop()
    pop.setValues(SE = n_SE, LTE = n_LTE)

    #return(n_SE, Ne, wave_mesh_arr, absorb_prof_arr, rate)
    return pop, Ne, l, rate


#--- calculate SE populations under fixed Ne
def SE_population_NeTe(atom, Ne, Te, backRad, Vd, Vt):
# return
#       n_SE[n_Level+1]        -- population
#       Nh                     -- hydrogen density
#       wave_mesh_arr[n_Line, n_wl]      -- wavelength  [cm]
#       absorb_prof_arr[n_Line, n_wl]    -- line profiles, normalized as integ_wl = 1

    Nh  = 2 * Ne  # [cm^{-3}]
    Ne0 = 1E-4 * Nh # イオン化バルブ効果
    nIter = 0

    n_LTE , nj_by_ni_Line, nj_by_ni_Cont = SELib.ni_nj_LTE(atom, Te, Ne)

    Rik, Rki_stim, Rki_spon = SELib.bf_R_rate(atom, Te, nj_by_ni_Cont, _backRad=backRad)
    Bij_Jbar, Bji_Jbar, wave_mesh_arr, absorb_prof_arr = SELib.B_Jbar(atom, Te, _Vt=Vt, _Vd=Vd, _Ne=Ne, _backRad=backRad)
    Cij = SELib.get_Cij(atom, Te)
    Cji = Collision.Cij_to_Cji( Cij, np.append(nj_by_ni_Line,nj_by_ni_Cont, axis=0) )

    n_SE = SELib.solve_SE(atom, Ne, Cji, Cij, Bji_Jbar, Bij_Jbar, Rki_spon, Rki_stim, Rik)

    Nh = Ne/(1E-4 + n_SE[-1,0])

#    print(f"Number of iteration   = {nIter}")


    wave_mesh_list = [ arr.squeeze() for arr in wave_mesh_arr ]
    absorb_prof_list = [ arr.squeeze() for arr in absorb_prof_arr ]
    n_SE = np.array(n_SE).squeeze()
    n_LTE = np.array(n_LTE).squeeze()

    rate = Rates()
    rate.setValues(Rik = Rik, Rki_stim = Rki_stim, Rki_spon = Rki_spon, Bij_Jbar = Bij_Jbar, Bji_Jbar = Bji_Jbar, Cij = Cij, Cji = Cji)

    l = LineProfs()
    nLine = len( wave_mesh_list )
    prof = [np.zeros(arr.shape[-1]) for arr in wave_mesh_list]#np.zeros([nLine,nWl])
    tau0 = np.zeros(nLine)
    intens = np.zeros(nLine)
    l.setValues(wl = wave_mesh_list, aprof = absorb_prof_list, prof = prof, tau0 = tau0, intens = intens)

    pop = LevelPop()
    pop.setValues(SE = n_SE, LTE = n_LTE)

    #return(n_SE, Ne, wave_mesh_arr, absorb_prof_arr, rate)
    return pop, Nh, l, rate
