

#-----------------------------------------------------------------------------
# modification history
#   2021/05/13 kouui
#     - added support for non-uniform wavelength meshgrid
#        wl, aprof, prof in class LineProfs are now list instead of array
#-----------------------------------------------------------------------------
# warnings:
#
#-----------------------------------------------------------------------------
# class:
#
# function:
#   - slab_0D_NhTe (atom, Nh, Te, backRad, Vd, Vt, D)
#   - slab_0D_NeTe (atom, Ne, Te, backRad, Vd, Vt, D)
#-----------------------------------------------------------------------------
import numpy as np

from .SE import SE_population_NhTe, SE_population_NeTe
from src.Math import Integrate

from ... import Constants as Cst
from ... import Config as Cfg


# SE line profiles for a uniform slab model
#   Nh, Te, D  ->  Ne, pop
def slab_0D_NhTe(atom, Nh, Te, backRad, Vd, Vt, D):

    pop, Ne, l, rates = SE_population_NhTe(atom, Nh, Te, backRad, Vd, Vt)

    nLine = len( l.wl )
    nLevel = pop.SE.shape[0]

    ## 1. obtain the upper/lower level population for line transitions
    nj = pop.SE[atom.Line.idxJ[:]]
    ni = pop.SE[atom.Line.idxI[:]]

    ## 2. compute extinction coefficient alpha
    nu = atom.Line.f0[:]           #  central freq.  [Hz]
    Bij = atom.Line.BIJ[:]
    Bji = atom.Line.BJI[:]
    alp0 = Cst.h_ * nu / (4. * Cst.pi_) * (Bij * ni - Bji * nj ) * Nh
    #alpha = alpha * absorb_prof_arr

    ## 3. compute line source function
    Aji = atom.Line.AJI[:]
    S = (Aji*nj) / (Bij * ni - Bji * nj)

    ## 4. compute optical depth given the thichness of the slab
    ## 5. compute the line profile
    for i in range(0,nLine):
        tau = D * alp0[i]*l.aprof[i][:]
        l.tau0[i] = np.max(tau)
        l.prof[i][:] = S[i] * (1. - np.exp(-tau[:]))
        l.intens[i] = Integrate.Trapze(l.prof[i][:], l.wl[i][:])

    return l, pop, Ne, rates

# SE line profiles for a uniform slab model
#   Ne, Te, D  ->  Nh, pop
def slab_0D_NeTe(atom,Ne,Te, backRad, Vd, Vt, D):

    pop, Nh, l, rates = SE_population_NeTe(atom, Ne, Te, backRad, Vd, Vt)

    nLine = len( l.wl )
    nLevel = pop.SE.shape[0]

    ## 1. obtain the upper/lower level population for line transitions
    nj = pop.SE[atom.Line.idxJ[:]]
    ni = pop.SE[atom.Line.idxI[:]]

    ## 2. compute extinction coefficient alpha
    nu = atom.Line.f0[:]           #  central freq.  [Hz]
    Bij = atom.Line.BIJ[:]
    Bji = atom.Line.BJI[:]
    alp0 = Cst.h_ * nu / (4. * Cst.pi_) * (Bij * ni - Bji * nj ) * Nh
    #alpha = alpha * absorb_prof_arr

    ## 3. compute line source function
    Aji = atom.Line.AJI[:]
    S = (Aji*nj) / (Bij * ni - Bji * nj)

    ## 4. compute optical depth given the thichness of the slab
    ## 5. compute the line profile
    for i in range(0,nLine):
        tau = D * alp0[i]*l.aprof[i][:]
        l.tau0[i] = np.max(tau)
        l.prof[i][:] = S[i] * (1. - np.exp(-tau[:]))
        l.intens[i] = Integrate.Trapze(l.prof[i][:], l.wl[i][:])

    return l, pop, Nh, rates
