################################################################################
# this file defines functions for
#     calculations related to basic/naive mathematical formula
################################################################################

from .. import Constants as Cst
from .. import Config as Cfg

import numba as nb


################################################################################
# If is an odd number, return True
################################################################################
def is_odd(num):
    r"""
    A fast method to check whether a number is odd.

    Parameters
    ----------

    num : int
        input

    Returns
    -------

    res : int
         1 if input num is an odd number, otherwise 0.
    """
    return num & 0x1

if Cfg.isJIT_:
    is_odd = nb.vectorize( [f'boolean({Cfg.dtsINT_})'], nopython=True )( is_odd )
