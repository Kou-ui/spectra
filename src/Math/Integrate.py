################################################################################
# this file defines functions for
#     integration method
################################################################################

import numpy
import numba

from .. import Constants as Cst
from .. import Config as Cfg

from . import BasicM



#-----------------------------------------------------------------------------
# Trapzoidal integral
#-----------------------------------------------------------------------------

def Trapze(y, x):
    r"""
    Integration using Trapzoidal rule

    Parameters
    ----------
    y : float, 1darray
        integrand as a function of variable x

    x : float, 1darray
        independent variable x

    Returns
    -------

    sum : float
         result of integration

    Notes
    ------
    Refer to [1]_.

    References
    ----------
    .. [1] Trapzoidal rule, wikipedia, https://en.wikipedia.org/wiki/Trapezoidal_rule
    """

    n = x.size
    dx = numpy.empty(n, dtype=Cfg.dtDOUBLE_)
    for i in range(1, n-1):
        dx[i] = 0.5 * (x[i+1]-x[i-1])
    dx[0] = 0.5 * (x[1]-x[0])
    dx[n-1] = 0.5 * (x[n-1] - x[n-2])

    sum = 0.0
    for i in range(n):
        sum += dx[i] * y[i]
    return sum

#-----------------------------------------------------------------------------
# simpson integral
#-----------------------------------------------------------------------------

def simps_odd_evenspaced(_y, _dx):
    r""" """
    _N = _y.size

    _result = _dx/3. * ( _y[0:_N-2:2] + 4*_y[1:_N-1:2] + _y[2:_N:2] )
    _result = numpy.sum(_result)

    return _result


def simps_odd(_y, _x):
    r""" """
    _N = _y.size

    _h = _x[1:] - _x[:-1]
    _h0 = _h[0:_N-2:2]
    _h1 = _h[1:_N-1:2]
    _hsum = _h0 + _h1
    _hprod = _h0 * _h1
    _h0divh1 = _h0 / _h1
    _result = _hsum/6. * ( _y[0:_N-2:2]*(2-1./_h0divh1) +
                           _y[1:_N-1:2]*_hsum*_hsum/_hprod +
                           _y[2:_N:2]*(2.-_h0divh1) )
    _result = numpy.sum( _result )

    return _result


def simps_even_evenspaced(_y, _dx):
    r""" """
    _N = _y.size

    _last_dx = _dx
    _first_dx = _dx
    _val = 0.
    _result = 0.

    _val += 0.5 * _last_dx * (_y[-2] + _y[-1])
    _result += simps_odd_evenspaced(_y[:_N-1], _dx)
    _val += 0.5 * _first_dx * (_y[0] + _y[1])
    _result += simps_odd_evenspaced(_y[1:], _dx)

    _result = 0.5 * (_result + _val)
    return _result

def simps_even(_y, _x):
    r""" """
    _N = _y.size

    _last_dx = _x[-1] - _x[-2]
    _first_dx = _x[1] - _x[0]
    _val = 0.
    _result = 0.

    _val += 0.5 * _last_dx * (_y[-2] + _y[-1])
    _result += simps_odd(_y[:_N-1], _x[:_N-1])
    _val += 0.5 * _first_dx * (_y[0] + _y[1])
    _result += simps_odd(_y[1:], _x[1:])

    _result = 0.5 * (_result + _val)
    return _result

def Simpson(_y, _x=None, _dx=None):
    r"""
    Integration using Simpson's rule

    Parameters
    ----------
    _y : float, 1darray
        integrand as a function of variable x

    _x : float, 1darray
        independent variable x

    _dx : float,
        inteval of _x if _x is evenly spaced

    Returns
    -------

    _sum : float
         result of integration
    """
    _N = _y.size

    if BasicM.is_odd(_N):

        if _dx is not None:
            return simps_odd_evenspaced(_y, _dx)
        elif _x is not None:
            return simps_odd(_y, _x)
        else:
            #raise ValueError("both _x and _dx are None")
            _dx = 1
            return simps_odd_evenspaced(_y, _dx)

    else:
        if _dx is not None:
            return simps_even_evenspaced(_y, _dx)
        elif _x is not None:
            return simps_even(_y, _x)
        else:
            #raise ValueError("both _x and _dx are None")
            _dx = 1
            return simps_even_evenspaced(_y, _dx)



################################################################################
# whether to compile them using numba's LLVM
################################################################################

if Cfg.isJIT_ == True:
    Trapze = numba.njit([f'{Cfg.dtsDOUBLE_}({Cfg.dtsDOUBLE_}[:],{Cfg.dtsDOUBLE_}[:])'])( Trapze )
    simps_odd_evenspaced = numba.njit([f"{Cfg.dtsDOUBLE_}({Cfg.dtsDOUBLE_}[:],{Cfg.dtsDOUBLE_})"])( simps_odd_evenspaced )
    simps_odd = numba.njit([f"{Cfg.dtsDOUBLE_}({Cfg.dtsDOUBLE_}[:],{Cfg.dtsDOUBLE_}[:])"])( simps_odd )
    simps_even_evenspaced = numba.njit([f"{Cfg.dtsDOUBLE_}({Cfg.dtsDOUBLE_}[:],{Cfg.dtsDOUBLE_})"])( simps_even_evenspaced )
    simps_even = numba.njit([f"{Cfg.dtsDOUBLE_}({Cfg.dtsDOUBLE_}[:],{Cfg.dtsDOUBLE_}[:])"])( simps_even )
