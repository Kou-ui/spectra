
from numpy import exp, cos, sin
import numpy
import numba

import sys
import timeit, functools, collections

sys.path.append('../../')

from src.Util import Benchmark


@numba.vectorize(['float64(float64)'],nopython=True)
def func_exp(u):
    z = u
    for k in range(100):
        z += exp(u)
    return z

@numba.vectorize(['float64(float64)'],nopython=True)
def func_exp2(u):
    z = u + 100 * exp(u)
    return z

@numba.vectorize(['float64(float64)'],nopython=True)
def func_sin(u):
    z = u
    for k in range(100):
        z += sin(u)
    return z

@numba.vectorize(['float64(float64)'],nopython=True)
def func_cos(u):
    z = u
    for k in range(100):
        z += cos(u)
    return z

@numba.vectorize(['float64(float64)'],nopython=True)
def func_add(u):
    z = u
    for k in range(100):
        z += u
    return z

@numba.vectorize(['float64(float64)'],nopython=True)
def func_mul(u):
    z = u
    for k in range(100):
        z *= u
    return z

@numba.vectorize(['float64(float64)'],nopython=True)
def func_div(u):
    z = u
    for k in range(100):
        z *= u
    return z

if __name__ == '__main__':

    _functions = collections.OrderedDict({
        "exp" : func_exp,
        "exp2" : func_exp2,
        "sin" : func_sin,
        "cos" : func_cos,
        "add" : func_add,
        "mul" : func_mul,
        "div" : func_div,
    })
    _result = collections.OrderedDict()
    for _key in _functions.keys():
        _result[_key] = []

    _nLoop = 10

    _ns = [100, 1000, 10000, 100000]

    for _n in _ns:

        print(f"n = {_n}")

        _x = numpy.random.rand(_n)[:]

        for _name, _fun in _functions.items():

            _ = _fun( _x)
            _t = timeit.Timer( functools.partial( _fun, _x) )
            _result[_name].append( _t.timeit(_nLoop)/_nLoop )

    Benchmark.print_table(_ns, _result, outFile='./result.txt')
