
import sys
sys.path.append('../..')

import numpy

import matplotlib.pyplot as plt

from src.Structure import AtomCls
from src.Atomic import BasicP

if __name__ == "__main__":

    ## select atom
    atom_, _ = AtomCls.InitAtom("../../data/conf/H.conf", isHydrogen=True)
    #atom_, _ = AtomCls.InitAtom("../../data/conf/Ca_II.conf", isHydrogen=False)


    Te_ = 1.E4
    Vt_ = 5.E5
    nbar_ = 30
    print('-'*nbar_)
    print(f"Te = {Te_:1.1E}")
    print(f"Vt = {Vt_:1.1E}")
    print('-'*nbar_)
    _qcore_A = 1.5
    _qwing_A = 10.

    print(f"wavelength [A] | qcore | qwing ")
    print('-'*nbar_)
    for k in range(atom_.nLine):
        w0_ = atom_.Line['w0'][k]

        _Dwidth = BasicP.get_Doppler_width(w0_, Te_, Vt_, atom_.Mass)
        _Dwidth *= 1E8

        _qwing = _qwing_A / _Dwidth
        _qcore = _qcore_A / _Dwidth

        print(f"{str(int(w0_*1E8)):^15s}|{str(int(_qcore)):^7s}|{str(int(_qwing)):^7s}")
    print('-'*nbar_)
