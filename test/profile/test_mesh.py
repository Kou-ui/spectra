
import sys
sys.path.append('../..')

import numpy

import matplotlib.pyplot as plt

from src.RadiativeTransfer.Profile import makeLineMesh_Full

if __name__ == "__main__":


    _nLambda = 101
    _qcore   = 2.5
    _qwing   = 10


    fig, ax = plt.subplots(1,1, figsize=(6,4), dpi=100)

    _mesh = makeLineMesh_Full(_nLambda, _qcore, _qwing)
    ax.plot( _mesh, '--o', markersize=3  )
    ax.axhline(_qcore)
    ax.axhline(-_qcore)
    ax.axhline(_qwing)
    ax.axhline(-_qwing)

    #plt.show()
    fig.savefig(sys.argv[0].replace('.py','.png'), dpi=100)
    plt.close()
