
import sys
sys.path.append('../..')

import numpy

import matplotlib.pyplot as plt

from src.RadiativeTransfer.Profile import makeLineMesh_Full
from src.RadiativeTransfer.Profile import Gaussian, Voigt

if __name__ == "__main__":


    _nLambda = 101

    fig, axs = plt.subplots(1,2, figsize=(10,4), dpi=120, sharey=True)


    ax = axs[0]
    _qcore   = 2.5
    _qwing   = 10
    _mesh = makeLineMesh_Full(_nLambda, _qcore, _qwing)
    _prof = Gaussian(_mesh)
    ax.plot( _mesh, _prof, '--o', markersize=3, label="Gaussian"  )
    for _a in (0.01, 0.1, 1.0):
        _prof = Voigt(_a, _mesh[:])
        ax.plot( _mesh, _prof, '--o', markersize=3, label=f"Voigt, a={_a:.2f}", markerfacecolor='None'  )

    ax.legend(loc="best")
    ax.set_xlabel("in uint of Doppler width")
    ax.set_ylabel("absorption probability")
    ax.set_title(f"qcore={_qcore}, qwing={_qwing}")

    ax = axs[1]
    _qcore   = 10
    _qwing   = 20
    _mesh = makeLineMesh_Full(_nLambda, _qcore, _qwing)
    _prof = Gaussian(_mesh)
    ax.plot( _mesh, _prof, '--o', markersize=3, label="Gaussian"  )
    for _a in (0.01, 0.1, 1.0):
        _prof = Voigt(_a, _mesh[:])
        ax.plot( _mesh, _prof, '--o', markersize=3, label=f"Voigt, a={_a:.2f}", markerfacecolor='None'  )

    ax.legend(loc="best")
    ax.set_xlabel("in uint of Doppler width")
    ax.set_ylabel("absorption probability")
    ax.set_title(f"qcore={_qcore}, qwing={_qwing}")

    #plt.show()
    fig.savefig(sys.argv[0].replace('.py','.png'), dpi=100)
    plt.close()
