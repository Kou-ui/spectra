import sys
sys.path.append('../..')

import numpy

import matplotlib.pyplot as plt

from src.Structure import AtomCls
from src.Atomic import BasicP

if __name__ == "__main__":

    atom_, _ = AtomCls.InitAtom("../../data/conf/Ca_II.conf", isHydrogen=False)


    Te_ = numpy.logspace(3, 5, 101)
    Vt_ = 5.E5

    fig, ax = plt.subplots(1,1, figsize=(9,5), dpi=100, sharey=True)

    for k in range(atom_.nLine):
        w0_ = atom_.Line['w0'][k]
        if w0_*1E8 > 10000:
            continue

        _Dwidth = BasicP.get_Doppler_width(w0_, Te_, Vt_, atom_.Mass)
        ax.plot( Te_, _Dwidth*1E8, label=f"{w0_*1E8:.0f}A")

    ax.legend(loc='best')
    ax.set_xlabel("Temperature, [$K$]")
    ax.set_xscale("log")

    ax.set_ylabel("Doppler width, [$\AA$]")
    ax.set_title("CaII")
    fig.savefig(sys.argv[0].replace('.py','.png'), dpi=100)
    plt.close()
    #plt.show()
