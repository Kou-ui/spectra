import sys
sys.path.append('../..')

import numpy

import matplotlib.pyplot as plt

from src.Structure import AtomCls
from src.Atomic import BasicP

if __name__ == "__main__":

    atom_, _ = AtomCls.InitAtom("../../data/conf/H.conf", isHydrogen=True)


    Te_ = numpy.logspace(3, 5, 101)
    Vt_ = 5.E5

    fig, axs = plt.subplots(1,2, figsize=(9,5), dpi=100, sharey=True)

    for k in range(atom_.nLine):
        w0_ = atom_.Line['w0'][k]
        ni_ = atom_.Line['ni'][k]
        nj_ = atom_.Line['nj'][k]

        if ni_ == 1:
            ax = axs[0]
            _Dwidth = BasicP.get_Doppler_width(w0_, Te_, Vt_, atom_.Mass)
            ax.plot( Te_, _Dwidth*1E8, label=f"{ni_}->{nj_}")

        if ni_ == 2:
            ax = axs[1]
            _Dwidth = BasicP.get_Doppler_width(w0_, Te_, Vt_, atom_.Mass)
            ax.plot( Te_, _Dwidth*1E8, label=f"{ni_}->{nj_}")

    for ax in axs:
        ax.legend(loc='best')
        ax.set_xlabel("Temperature, [$K$]")
        ax.set_xscale("log")

    axs[0].set_ylabel("Doppler width, [$\AA$]")
    fig.suptitle("HI")
    fig.savefig(sys.argv[0].replace('.py','.png'), dpi=100)
    plt.close()
