
import h5py
import warnings
warnings.simplefilter("ignore")

import numpy

def write():

    #-------------------------------------------------------------------------
    # create group and dataset
    #-------------------------------------------------------------------------

    with h5py.File("/Users/liu/Desktop/testfile.hdf5", 'a') as f:

        group1 = f.create_group("observation")
        group1.attrs["description"] = "scientific observation"
        group1.attrs["kind"] = "spectro-polametry"

        dataset = group1.create_dataset( name="1", data=numpy.random.randn(10,10).astype('float32') )
        dataset.attrs["description"] = "filament observation"
        dataset.attrs["exposure[msec]"] = 200
        dataset.attrs["datetime"] = "2020-09-01 10.00.00.123"
        dataset.attrs["wavelength[AA]"] = 6303
        dataset.attrs["period[sec]"] = 4
        dataset.attrs["target"] = 'filament'

        group2 = f.create_group("calibration")
        group2.attrs["description"] = "calibration of DST Mueller Matrix"
        group2.attrs["kind"] = "spectro-polametry"

        dataset = group2.create_dataset( name="2", data=numpy.random.randn(10,10).astype('float32') )
        dataset.attrs["description"] = "with turret"
        dataset.attrs["exposure[msec]"] = 200
        dataset.attrs["datetime"] = "2020-09-02 10.00.00.123"
        dataset.attrs["wavelength[AA]"] = 6303
        dataset.attrs["period[sec]"] = 4
        dataset.attrs["target"] = "diskcenter"
        dataset.attrs["angle[deg]"] = 360.0

        dataset = group2.create_dataset( name="3", data=numpy.random.randn(10,10).astype('float32') )
        dataset.attrs["description"] = "with polarizer"
        dataset.attrs["exposure[msec]"] = 200
        dataset.attrs["datetime"] = "2020-09-04 10.00.00.123"
        dataset.attrs["wavelength[AA]"] = 6303
        dataset.attrs["period[sec]"] = 4
        dataset.attrs["target"] = "lamp"
        dataset.attrs["angle[deg]"] = 45.0

def read():

    #-------------------------------------------------------------------------
    # create group and dataset
    #-------------------------------------------------------------------------

    with h5py.File("/Users/liu/Desktop/testfile.hdf5", 'r') as f:

        for key in f.keys():
            print(key)

        for key in f["calibration"].keys():
            print(key)

        ds = f["calibration"]["2"]
        print(ds.value, ds.dtype, ds.attrs)



if __name__ == "__main__":

    read()
